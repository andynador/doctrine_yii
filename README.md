Yii + Doctrine
=============================
Здесь я потихоньку пытаюсь заинтегрировать Yii 1 и ORM Doctrine.

Вот небольшой список того, что есть:

1) Создание миграций на основе созданных сущностей
Сначала вызываем команду
```
#!bash

protected/yiic doctrineorm m:u --configuration=protected/config/migration.yml
```
которая определяет изменения в БД и создаёт миграцию с ними. 

Далее вызываем команду
```
#!bash

protected/yiic doctrineorm m:m --configuration=protected/config/migration.yml
```
Которая применяет созданные миграции в БД. При этом никакого SQL-кода писать не потребовалось, изменения были определены автоматически.

2) Можно протестировать некоторые выборки и создание пользователей
```
#!bash

protected/yiic testdoctrine getOneUser - получение пользователя с логином 'admin'
protected/yiic testdoctrine getAllUsers - получение всех пользователей, отсортированных по логину. Пример примечателен тем, что в нём используется кастомный метод из репозитория для модели User, а также запрос сформирован с помощью DQL
protected/yiic testdoctrine createAdmin - создание пользователя с логином 'admin'
protected/yiic testdoctrine createSimpleUser - создание пользователя с логином 'simple'
```