<?php
    return [
        'class' => 'application\\components\\DoctrineComponent',
        'proxyPath' => __DIR__ . '/../proxies',
        'pathToYamlFiles' => __DIR__ . '/doctrine/yaml',
        'driver' => 'pdo_pgsql',
        'user' => 'postgres',
        'password' => 'postgres',
        'host' => 'localhost',
        'dbname' => 'doctrine_local'
    ];