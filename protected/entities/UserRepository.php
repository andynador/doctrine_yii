<?php

namespace application\entities;

/**
 * UserRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class UserRepository extends \Doctrine\ORM\EntityRepository
{
    public function findAllOrderedByName()
    {
        return $this->getEntityManager()
        ->createQuery('SELECT p FROM ' . User::class . ' p ORDER BY p.name ASC')
        ->getResult();
    }
}
