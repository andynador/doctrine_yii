<?php
namespace application\components;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\Mapping\Driver\YamlDriver;
use Doctrine\Common\Cache\FilesystemCache;
use Yii;

class DoctrineComponent extends \CApplicationComponent
{
    /**
     * @var EntityManager
     */
    private $em = null;

    private $pathToYamlFiles;    

    private $proxyPath;

    private $driver;

    private $user;

    private $password;

    private $host;

    private $dbname;

    public function init()
    {
        $this->initDoctrine();
        parent::init();
    }

    private function initDoctrine()
    {   
        $cache = new FilesystemCache(Yii::app()->runtimePath . '/cache');
        $config = new Configuration();
        $config->setMetadataCacheImpl($cache);

        $driver = new YamlDriver($this->getPathToYamlFiles());
        $config->setMetadataDriverImpl($driver);

        $config->setMetadataDriverImpl($driver);
        $config->setQueryCacheImpl($cache);
        $config->setProxyDir($this->getProxyPath());
        $config->setProxyNamespace('Proxies');
        $config->setAutoGenerateProxyClasses(true);
        $connectionOptions = array(
            'driver' => $this->getDriver(),
            'user' => $this->getUser(),
            'password' => $this->getPassword(),
            'host' => $this->getHost(),
            'dbname' => $this->getDbname()
        );
        
        $this->em = EntityManager::create($connectionOptions, $config);
    }

    public function setPathToYamlFiles($pathToYamlFiles)
    {
        $this->pathToYamlFiles = $pathToYamlFiles;
    }

    public function getPathToYamlFiles()
    {
        return $this->pathToYamlFiles;
    }

    public function setProxyPath($proxyPath)
    {
        $this->proxyPath = $proxyPath;
    }

    public function getProxyPath()
    {
        return $this->proxyPath;
    }

    public function setDbname($dbname)
    {
        $this->dbname = $dbname;
    }

    public function getDbname()
    {
        return $this->dbname;
    }

    public function setDriver($driver)
    {
        $this->driver = $driver;
    }

    public function getDriver()
    {
        return $this->driver;
    }

    public function setHost($host)
    {
        $this->host = $host;
    }

    public function getHost()
    {
        return $this->host;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setUser($user)
    {
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }

    /**
     *
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->em;
    }
}