<?php
use application\entities\User;

class TestDoctrineCommand extends CConsoleCommand
{
    public function actionCreateAdmin()
    {
        $user = new User();
        $user->setName('admin');
        $user->setPassword('admin');
        $user->setEmail('admin@admin.ru');
        $user->setRole('admin');
        $user->setCreated(new \DateTime());
        $user->setModified(new \DateTime());
        Yii::app()->doctrine->getEntityManager()->persist($user);
        Yii::app()->doctrine->getEntityManager()->flush();
    }
    
    public function actionCreateSimpleUser()
    {
        $user = new User();
        $user->setName('simple');
        $user->setPassword('simple');
        $user->setEmail('simple@simple.ru');
        $user->setRole('simple');
        $user->setCreated(new \DateTime());
        $user->setModified(new \DateTime());
        Yii::app()->doctrine->getEntityManager()->persist($user);
        Yii::app()->doctrine->getEntityManager()->flush();
    }
    
    public function actionGetAllUsers()
    {
        $all = Yii::app()->doctrine->getEntityManager()->getRepository(User::class)->findAllOrderedByName();
        print_r($all);
    }
    
    public function actionGetOneUser()
    {
        $model = Yii::app()->doctrine->getEntityManager()->getRepository(User::class)->findOneByName('admin');
        print_r($model);
    }
}